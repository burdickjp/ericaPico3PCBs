# Erica Synths Pico System III Preset PCBs

This repository is for the KiCAD files necessary to produce an Erica Pico System III preset PCB which makes the connections shown here:  
https://patch-library.net/patches/14eb7145-5006-41a1-9d44-3a70bfda3765

NOISE ⟶ MIX3 IN 2  
VCO1 TRI ⟶ MIX3 IN 1  
VCO2 SHAPE ⟶ EXP FM  
EG1 OUT ⟶ MIX1 IN 1  
EG2 OUT ⟶ MIX2 IN 1  
NOISE ⟶ MIX2 IN 3  
MOD SINE ⟶ MIX2 IN 2  
MOD SINE ⟶ MIX1 IN 2  
S&H ⟶ MIX1 IN 3  
MOD PULSE ⟶ CLK IN  
SEQ CV OUT ⟶ VCO1 1V/OCT  
EG1 OUT ⟶ IN/LIN FM  
MIX1 OUT ⟶ LPG1 CV2  
VCO2 TRI ⟶ LPG1 IN  
LPG2 OUT ⟶ BBD IN  

To use the PCB you'll need to make these connections:  
https://patch-library.net/patches/c1d4c8a4-a5c3-4667-9b12-9a8e8f1eb2d9

MIX2 OUT ⟶ LPG2 CV2  
MIX3 OUT ⟶ LPG2 IN  

## License

This repository follows the CC BY-NC-SA 4.0 license

